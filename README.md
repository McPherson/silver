**SILVER**

The Strategic Integration of Large-capacity Variable Energy Resources (SILVER) tool is a generic electricity network optimization tool written in Python based on object-oriented programming. It has been designed to be adaptable in different dimensions: temporal, spatial, technology representation and market design. 

Currently SILVER is tested with Python 3.9.16


**Getting Started**

Please read SILVER user manual PDF available in SILVER folder


SILVER was developed by [the Sustainable Energy Systems Integration & Transitions Group](https://sesit.cive.uvic.ca/)

**Authors**


- Madeleine McPherson
- Mohammadali Saffari
- Mohammad Miri
- Jacob Monroe
- Evan Dungate
- Ahnaf Ahmed



**Note**

SILVER is split up into 4 stages. 
- Inputs processing
- Price Setting OPF
- Unit Commitment
- Realtime OPF


Input processing is run first followed by a loop of price setting opf then unit commitment, then realtime opf. The loop will run for each hour commitment period in your specified time range that you can set in the configVariables.ini (SILVER/SILVER_DATA/user_inputs/configVariables.ini) file.

Within price setting opf and realtime opf, LPs will be created and solved for each hour in the commit period


Input file examples are contained in the repo with the SS scenario name


**Environment Setup:**

1. install requirements
    1. [CPLEX](https://www.ibm.com/products/ilog-cplex-optimization-studio/cplex-optimizer)
    2. Python 3.9.16 (Installed automatically if using conda, needed if using venv)
    3. Optional: [Anaconda](https://docs.anaconda.com/anaconda/install/index.html)
    
    NOTE: you can use [glpk](https://winglpk.sourceforge.net/) as a free alternative, however the solving capacity is limited.


2. If you are using Anaconda, open up an **anaconda prompt** otherwise open up your terminal of choice.

3. Next, navigate your file system using the cd command until you are in the directory containing [requirements.txt](https://gitlab.com/McPherson/silver/-/blob/main/requirements.txt)

4. Create a virtual environment for SILVER's packages
    * If using Anaconda:
        1. In this directory, run the command
        ```console
            conda create -n silver_env python=3.9  
        ```
        2. Once the environment is finished with its setup, activate the environment with the command
        ```console
            conda activate silver_env
        ```

    * If using venv:
        1. In this directory, run the command
        ```console
            python -m venv .venv
        ```
        2. Once the environment is finished with its setup, activate the environment. The command may differ depending on your terminal as follows
   
        | Terminal       | Command                      |
        |----------------|------------------------------|
        | Command prompt | `.venv\Scripts\activate.bat` |
        | Powershell     | `.venv\Scripts\Activate.ps1` |
        | Bash/git bash  | `source .venv/bin/activate`  |
         
5. Now that the environment is activated add the modules with
```console
    pip install -r requirements.txt
```

6. To access this environment in the future, repeat steps 2 and 4.2

**Scenario Setup**

1. In a text editor open [configVariables.ini](https://gitlab.com/McPherson/silver/-/blob/main/silver/SILVER_Data/user_inputs/configVariables.ini). This file contains a preloaded user configuration that will run a 48 hour simulation solved using cplex solver with the provided scenario "SS" data. To change these parameters specifically the solver used if you are using GLPK. Set the variables as follows:  
    - **user_startdate** this value defines the starting date of the scenario. the program will begin by looking for data that begins at 12am on this date. Must be in the format yyyy/m/d and the startdate must be within the range of user provided input data in [<Scenario number>_Demand_Real_Forecasted.xlsx](https://gitlab.com/McPherson/silver/-/blob/main/silver/SILVER_Data/user_inputs/SS_Demand_Real_Forecasted.xlsx)  
    - **user_enddate** must be in the format yyyy/m/d and the enddate must be within the range of user provided input data in [<Scenario number>_Demand_Real_Forecasted.xlsx](https://gitlab.com/McPherson/silver/-/blob/main/silver/SILVER_Data/user_inputs/SS_Demand_Real_Forecasted.xlsx)  
    - **hours_commitment** this value defines the range of hours commitment to solve for the UC and OPF problems. Recommended ranges for testing user input are 24 or 48. For complete runs we recommend using 168 for weekly hour commitment and 720 for monthly hour commitment. Note the hours_commitment must fit within the provided start and end date
    - **scenario_number** this value is used to define distinct user defined scenarios. To create a new scenario, change this value to a string of your choosing and ensure that it matches the other user input values including:
        - model inputs - **scenario_number**.xlsx
        - **scenario_number**_Demand_Real_Forecasted.xlsx
        - Hydro_Data-**scenario_number**
        - VRE_Resource_analysis-**scenario_number**
    - **runopf** this boolean value when set to True will run both OPF and UC stages of the model. When set to false only the UC stage of the model will be run.
    - **mustrun_ngcc_bio** when this is set to True, 'NG_CC', 'NG_CT', 'NG_CG', 'biomass', 'nuclear' type generators are considered must run. When false these generators can be turned off.
    - **uc_networked** when set to True the UC stage will consider transmission line constraints
    - **merra_data_year** specifies the merra data used for VRE schedules, this is present in filepath to VRE generation data e.g. in the [solar generation for "SS" example data the year is 2018](https://gitlab.com/McPherson/silver/-/tree/main/silver/SILVER_Data/user_inputs/VRE_Resource_Analysis-SS/solar_generation_data/282-96).
    - **solver** this determines which solver will be used. Options: glpk or cplex

2. Once your configuration is set activate the environment created in stage one using the command 
```console
    conda activate silver_env
```

3. Next navigate a command prompt in the silver_env environment to ..\silver\SILVER_Code where you will find SILVER_Runner.py

3. Run the command  
```console
    python SILVER_Runner.py
```
4. If you have not changed the scenario_number variable in configVariables.ini, a pre loaded configuration of silver with the scenario name "SS" should run.

5. To configure your own silver scenario further such as adding additional generators, transmission lines, demand schedule etc. change the relevant input files in the user_inputs folder. Details on the user input sheets are contained in the user [manual](https://gitlab.com/McPherson/silver/-/blob/main/SILVER%20-%20User-manual%20-%20Open%20Access%20Version%20-28%20NOV%202022.pdf). 
